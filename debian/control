Source: konversation
Section: net
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Aurélien COUDERC <coucouf@debian.org>,
           Pino Toscano <pino@debian.org>,
Build-Depends: debhelper-compat (= 13),
               dh-python,
               cmake (>= 3.16.0~),
               extra-cmake-modules (>= 6.3.0~),
               gettext,
               libkf6archive-dev (>= 6.3.0~),
               libkf6bookmarks-dev (>= 6.3.0~),
               libkf6codecs-dev,
               libkf6config-dev (>= 6.3.0~),
               libkf6configwidgets-dev (>= 6.3.0~),
               libkf6coreaddons-dev (>= 6.3.0~),
               libkf6crash-dev (>= 6.3.0~),
               libkf6dbusaddons-dev (>= 6.3.0~),
               libkf6doctools-dev (>= 6.3.0~),
               libkf6globalaccel-dev (>= 5.74.0~),
               libkf6i18n-dev (>= 6.3.0~),
               libkf6idletime-dev (>= 6.3.0~),
               libkf6itemviews-dev (>= 6.3.0~),
               libkf6kio-dev (>= 6.3.0~),
               libkf6newstuff-dev (>= 6.3.0~),
               libkf6notifications-dev (>= 6.3.0~),
               libkf6notifyconfig-dev (>= 6.3.0~),
               libkf6parts-dev (>= 6.3.0~),
               libkf6statusnotifieritem-dev (>= 6.3.0~),
               libkf6textwidgets-dev (>= 6.3.0~),
               libkf6wallet-dev (>= 6.3.0~),
               libkf6widgetsaddons-dev (>= 6.3.0~),
               libkf6windowsystem-dev (>= 6.3.0~),
               libphonon4qt6-dev,
               libphonon4qt6experimental-dev,
               libqca-qt6-dev (>= 2.2.0~),
               pkg-kde-tools,
               python3:any,
               qt6-5compat-dev (>= 6.4.0~),
               qt6-base-dev (>= 6.4.0~),
               qt6-multimedia-dev (>= 6.4.0~),
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://konversation.kde.org/
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/konversation.git
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/konversation

Package: konversation
Architecture: any
Depends: konversation-data (= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Provides: irc,
Description: user friendly Internet Relay Chat (IRC) client for KDE
 Konversation is a client for the Internet Relay Chat (IRC) protocol.
 It is easy to use and well-suited for novice IRC users, but novice
 and experienced users alike will appreciate its many features:
 .
       * Standard IRC features
       * Easy to use graphical interface
       * Multiple server and channel tabs in a single window
       * IRC color support
       * Pattern-based message highlighting and OnScreen Display
       * Multiple identities for different servers
       * Multi-language scripting support (with DCOP)
       * Customizable command aliases
       * NickServ-aware log-on (for registered nicknames)
       * Smart logging
       * Traditional or enhanced-shell-style nick completion
       * DCC file transfer with resume support

Package: konversation-data
Architecture: all
Depends: ${misc:Depends}, ${perl:Depends}, ${python3:Depends},
Recommends: konversation (>= ${source:Version}),
Description: data files for Konversation
 Konversation is a client for the Internet Relay Chat (IRC) protocol. This
 package contains data files for Konversation. It is probably of no use if
 `konversation' package is not installed.
